# rp2040_vscode_template

A starting point for working with rp2040 in vscode. You will need to set up your pico sdk path in `.vscode/c_cpp_properties.json`.  This will compile, and you should be able to load the uf2 file onto a pico.  The default led will blink. 